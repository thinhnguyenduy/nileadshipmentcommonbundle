<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:30 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Message;

use Nilead\ShipmentComponent\Model\ShippingSubjectInterface;
use Nilead\ShipmentCommonComponent\Message\RequestInterface;
use Nilead\ShipmentCommonComponent\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use GuzzleHttp\ClientInterface;

abstract class RequestAbstract implements RequestInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    protected $parameters;

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $httpRequest;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var ShippingSubjectInterface
     */
    protected $package;

    /**
     * @var string
     */
    protected $liveEndpoint;

    /**
     * @var string
     */
    protected $developerEndpoint;


    /**
     * Create a new Request
     *
     */
    public function __construct()
    {
        $this->parameters = new ParameterBag;
    }

    /**
     * {@inheritdoc}
     */
    public function setPackage(ShippingSubjectInterface $package)
    {
        $this->package = $package;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * {@inheritdoc}
     */
    public function setServices(ClientInterface $httpClient, HttpRequest $httpRequest)
    {
        $this->httpClient = $httpClient;
        $this->httpRequest = $httpRequest;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(ShippingSubjectInterface $package, array $parameters = array())
    {
        if (null !== $this->response) {
            throw new \RuntimeException('Request cannot be modified after it has been sent!');
        }

        $this->parameters = new ParameterBag;

        $this->setParameters($parameters);

        $this->package = $package;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setParameters(array $parameters = array())
    {
        foreach ($parameters as $key => $value) {
//            // TODO: perhaps we dont allow setting params that dont exist?
//            if (isset($parameters[$key])) {
//                $value = $parameters[$key];
//            }

            if (is_array($value)) {
                $this->parameters->set($key, reset($value));
            } else {
                $this->parameters->set($key, $value);
            }
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultParameters()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getParameters()
    {
        return $this->parameters->all();
    }

    /**
     * {@inheritdoc}
     */
    public function send()
    {
        $data = $this->getData();

        return $this->sendData($data);
    }

    /**
     * {@inheritdoc}
     */
    public function getEndpoint()
    {
        return $this->getTestMode() ? $this->developerEndpoint : $this->liveEndpoint;
    }

    /**
     * {@inheritdoc}
     */
    public function getTestMode()
    {
        return $this->getParameter('testMode');
    }

    /**
     * {@inheritdoc}
     */
    public function setTestMode($value)
    {
        return $this->setParameter('testMode', $value);
    }

    /**
     * {@inheritdoc}
     */
    public function validate()
    {
        foreach (func_get_args() as $key) {
            $value = $this->parameters->get($key);
            if (empty($value)) {
                throw new \Exception("The $key parameter is required");
            }
        }

        return true;
    }


    protected function getParameter($key)
    {
        return $this->parameters->get($key);
    }

    protected function setParameter($key, $value)
    {
        if (!isset($this->getDefaultParameters()[$key])) {
            throw new \RuntimeException(sprintf('Cannot set undefined parameter %s!', $key));
        }

        $this->parameters->set($key, $value);

        return $this;
    }
}
