<?php
/**
 * Created by Rubikin Team.
 * Date: 5/23/14
 * Time: 2:22 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Message;

use Nilead\ShipmentComponent\Exception\RatesRequestException;
use Nilead\ShipmentComponent\Model\ShippingSubjectInterface;

trait RatesRequestTrait
{
    protected $methods = array();

    public function setMethods(array $methods)
    {
        $this->methods = $methods;

        return $this;
    }

    public function getMethods()
    {
        return $this->methods;
    }

    public function initialize(ShippingSubjectInterface $package, array $parameters = array())
    {
        if (!isset($parameters['method_codes'])) {
            throw new RatesRequestException('Missing parameter `method_codes`');
        }

        $this->setMethods($parameters['method_codes']);

        unset($parameters['method_codes']);

        return parent::initialize($package, $parameters);
    }

} 